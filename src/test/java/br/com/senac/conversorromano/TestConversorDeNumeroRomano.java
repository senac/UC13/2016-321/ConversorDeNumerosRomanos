package br.com.senac.conversorromano;

import org.junit.Assert;
import org.junit.Test;

public class TestConversorDeNumeroRomano {

    @Test
    public void deveEntenderOSimboloI() {
        int numero = ConversorDeNumeroRomano.converter("I");
        Assert.assertEquals(1, numero);
    }

    @Test
    public void deveEntenderOSimboloV() {
        int numero = ConversorDeNumeroRomano.converter("V");
        Assert.assertEquals(5, numero);
    }

    @Test
    public void deveEntenderOSimboloX() {
        int numero = ConversorDeNumeroRomano.converter("X");
        Assert.assertEquals(10, numero);
    }

    @Test
    public void deveEntenderOSimboloL() {
        int numero = ConversorDeNumeroRomano.converter("L");
        Assert.assertEquals(50, numero);
    }

    @Test
    public void deveEntenderOSimboloC() {
        int numero = ConversorDeNumeroRomano.converter("C");
        Assert.assertEquals(100, numero);
    }

    @Test
    public void deveEntenderOSimboloD() {
        int numero = ConversorDeNumeroRomano.converter("D");
        Assert.assertEquals(500, numero);
    }

    @Test
    public void deveEntenderOSimboloM() {
        int numero = ConversorDeNumeroRomano.converter("M");
        Assert.assertEquals(1000, numero);
    }

    @Test
    public void deveEntenderODoisSimbolosIguaiII() {
        int numero = ConversorDeNumeroRomano.converter("II");
        Assert.assertEquals(2, numero);

    }

    @Test
    public void deveEntenderODoisSimbolosDiferentesIV() {
        int numero = ConversorDeNumeroRomano.converter("IV");
        Assert.assertEquals(4, numero);

    }

    @Test
    public void deveEntenderNumeroComplexoXXIV() {
        int numero = ConversorDeNumeroRomano.converter("XXIV");
        Assert.assertEquals(24, numero);
    }

    @Test
    public void deveEntenderNumeroMegaComplexoCMXCIX() {
        int numero = ConversorDeNumeroRomano.converter("CMXCIX");
        Assert.assertEquals(999, numero);
    }

}
