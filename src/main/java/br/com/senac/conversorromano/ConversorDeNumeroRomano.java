package br.com.senac.conversorromano;

import java.util.HashMap;
import java.util.Map;

public class ConversorDeNumeroRomano {

    private static Map<Character, Integer> tabela = new HashMap<Character, Integer>() {
        {
            put('I', 1);
            put('V', 5);
            put('X', 10);
            put('L', 50);
            put('C', 100);
            put('D', 500);
            put('M', 1000);
        }
    };

    public static int converter(String numeroRomano) {

        int acumulador = 0;
        int ultimoVizinhoDaDireita = 0;

        for (int i = numeroRomano.length() - 1; i >= 0; i--) {

            //pegar o valor inteiro ao simbolo
            int atual = tabela.get(numeroRomano.charAt(i));
            int multilpicador = 1;

            if (atual < ultimoVizinhoDaDireita) {
                multilpicador = -1;
            }

            acumulador += tabela.get(numeroRomano.charAt(i)) * multilpicador;

            ultimoVizinhoDaDireita = atual;

        }

        return acumulador;

    }

}
